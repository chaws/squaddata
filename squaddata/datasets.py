import pandas as pd
from squad_client.core.models import Environment, Suite
from squad_client.utils import getid


def changes(group, project, build, base_build):
    changes = project.compare_builds(base_build.id, build.id)

    change_data = []
    for change in changes.keys():
        for environment in changes[change].keys():
            for suite, tests in changes[change][environment].items():
                for test in tests:
                    change_data.append(
                        {
                            "group": group.slug,
                            "project": project.slug,
                            "build": build.version,
                            "base_build": base_build.version,
                            "environment": environment,
                            "suite": suite,
                            "test": test,
                            "change": change,
                        }
                    )

    data = pd.DataFrame(change_data)

    if not data.empty:
        data.sort_values(by=["environment", "suite", "test"], inplace=True)

    return data


def results(group, project, build, base_build):
    tests = build.tests().values()

    result_data = []
    for test in tests:
        environment = Environment().get(_id=getid(test.environment))
        suite = Suite().get(_id=getid(test.suite))

        result_data.append(
            {
                "group": group.slug,
                "project": project.slug,
                "build": build.version,
                "base_build": base_build.version,
                "environment": environment.slug,
                "suite": suite.slug,
                "test": test.short_name,
                "status": test.status,
            }
        )

    data = pd.DataFrame(result_data)
    data.sort_values(by=["environment", "suite", "test"], inplace=True)

    return data
