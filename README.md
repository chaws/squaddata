# squaddata
Create datasets and reports from [squad](https://github.com/Linaro/squad)

## Installation
Use [pip](https://pip.pypa.io/en/stable/) to install from [gitlab](https://gitlab.com/Linaro/lkft/reports/squaddata)
```
pip install https://gitlab.com/Linaro/lkft/reports/squaddata/-/archive/master/squaddata-master.tar.gz
```

## Usage
```
usage: squad_report [-h] [--url URL] [--token TOKEN]
       [--group GROUP] [--project PROJECT] [--build BUILD] [--base-build BASE_BUILD]
       [--environments ENVIRONMENTS | --environment-prefixes ENVIRONMENT_PREFIXES]
       [--suites SUITES | --suite-prefixes SUITE_PREFIXES]
       report

Create a report using data from SQUAD

positional arguments:
  report                Type of report to create

optional arguments:
  -h, --help            show this help message and exit
  --url URL             URL of the SQUAD service
  --token TOKEN         Authentication token of the SQUAD service
  --group GROUP         SQUAD group
  --project PROJECT     SQUAD project
  --build BUILD         SQUAD build
  --base-build BASE_BUILD
                        SQUAD build to compare to
  --environments ENVIRONMENTS
                        List of SQUAD environments to include
  --environment-prefixes ENVIRONMENT_PREFIXES
                        List of prefixes of SQUAD environments to include
  --suites SUITES       List of SQUAD suites to include
  --suite-prefixes SUITE_PREFIXES
                        List of prefixes of SQUAD suites to include
```

## Contributing
This project is run at [gitlab](https://gitlab.com/Linaro/lkft/reports/squaddata)

Please open any issues or merge requests there.

## License
[MIT](https://gitlab.com/Linaro/lkft/reports/squaddata/-/blob/master/LICENSE)
